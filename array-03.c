//Write a program to add an multiply two given matrixes.

#include <stdio.h>
void Mtrx(int mtrx[][16], int cols,int rows);
void multiMtrx(int firsMtrx[][16], int seconMtrx[][16], int res[][16],int rw1,int rw2,int cm1, int cm2);




int main()
{
    printf("A program to add and multiply two given numbers\n");
    printf("------------------------------------------------\n");
    int firsMtrx[16][16],seconMtrx[16][16], res[16][16], rw1,rw2,cm1,cm2;
    printf("\nEnter the Rows and Columns of the First Matrix=");
    scanf("%d%d", &rw1, &cm1);
    printf("Enter the Rows and Columns of the Second Matrix=");
    scanf("%d%d", &rw2, &cm2);
    while (cm1!=rw2)
    {
        printf("Cannot be calculated.Re-enter the Rows and Columns.\n");
        printf("Enter the Rows and Columns of the First Matrix: ");
        scanf("%d%d", &rw1, &cm1);
        printf("Enter Rows and Columns of the Second Matrix: ");
        scanf("%d%d", &rw2, &cm2);
    }
    Mtrx(firsMtrx, rw1, cm1);
    Mtrx(seconMtrx, rw2, cm2);
    multiMtrx(firsMtrx, seconMtrx, res, rw1, cm1, rw2, cm2);
    answr(res, rw1, cm2);

    return 0;
}

void multiMtrx(int firsMtrx[][16],int seconMtrx[][16],int res[][16],int rw1, int cm1, int rw2, int cm2)
{
    for (int k = 0; k < rw1; ++k)
    {
        for (int q = 0; q < cm2; ++q)
        {
            res[k][q] = 0;
        }
    }

    for (int m = 0; m < rw1; ++m)
    {
        for (int n = 0; n < cm2; ++n)
        {
            for (int p = 0; p < cm1; ++p)
            {
                res[m][n] = res[m][n]+firsMtrx[m][p] * seconMtrx[p][n];
            }
        }
    }
}

void Mtrx(int mtrx[][16], int rows, int cols)
{

    printf("\nEnter your elements of  matrix=\n");

    for (int i = 0; i < rows; ++i)
    {
        for (int j = 0; j < cols; ++j)
        {
            printf("Enter a%d%d: ", i + 1, j + 1);
            scanf("%d", &mtrx[i][j]);
        }
    }
}

void answr(int res[][16], int rows, int cols)
{

    printf("\n Answer=\n");
    for (int z = 0; z < rows; ++z)
    {
        for (int t = 0; t < cols; ++t)
        {
            printf("%d\t", res[z][t]);
            if (t == cols - 1)
                printf("\n");
        }
    }
}


