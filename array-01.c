//Write a C program to reverse a sentence entered by user.
#include <stdio.h>
#define SIZE 100
void senReverse();

void senReverse()
{
    char sen[SIZE];
    scanf("%c",&sen[SIZE]);
    if (sen[SIZE]!= '\n')
    {
        senReverse();
        printf("%c", sen[SIZE]);
    }
}

int main()
{   printf("A program to reverse a sentence entered by the user\n");
    printf("---------------------------------------------------\n");
    printf("Enter your sentence=\n");
    senReverse();
    return 0;
}
