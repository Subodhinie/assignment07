//Write a program to find the Frequency of a given character in a given string.
#include<stdio.h>
#include <string.h>
#define SIZE 1000

int main()
{
    char w[SIZE];
    int  i=0,j,counLetter=0,num;
    printf("Enter your string=\n");
    fgets(w,SIZE,stdin);
    while(w[i])
    {
        num=i;
        i++;
    }
    printf("Frequency of the characters in the entered string=\n");
    for(j=0; j<num; j++)
    {
        counLetter=1;
        if(w[j])
        {
            for(i=j+1; i<num; i++)
            {

                if(w[j]==w[i])
                {
                    counLetter++;
                    w[i]='\0';
                }
            }
            printf(" '%c'= %d \n",w[j],counLetter);
        }
    }
    return 0;
}

